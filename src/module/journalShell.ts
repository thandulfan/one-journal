import { getSetting, setSetting, settings } from "./settings.js";
import { i18n, scrollElementIntoView } from "./util.js";

interface IHistoryItem {
  id: string;
  title?: string;
  hash: string;
}

export class JournalShell extends Application {
  _state: number;

  get element(): JQuery<HTMLElement> {
    return super.element as JQuery<HTMLElement>;
  }

  get document(): Document {
    return this.element.get(0).ownerDocument;
  }

  directory: OneJournalDirectory;
  attachedId: number = -1;
  attachedUid: string;
  history: IHistoryItem[] = [];
  historyFwd: IHistoryItem[] = [];
  historySeq = 0;
  contextMenu: ContextMenu;
  detachedJournals: Set<string> = new Set();
  swappingJournals: Set<string> = new Set();

  constructor() {
    super({
      id: "OneJournalShell",
      template: "modules/one-journal/templates/shell.html",
      title: "OneJournal",
      classes: [`oj-${game.system.id}`],
      popOut: true,
      resizable: true,
      width: 850,
      height: 600,
    });

    window.onhashchange = e => {
      if (getSetting(settings.USE_BROWSER_HISTORY) !== true) return;
      const hash = window.location.hash;
      const i = this.history.findIndex(item => item.hash === hash);
      if (i !== -1) {
        this.goToHistoryByIndex(i);
      } else {
        const i = this.historyFwd.findIndex(item => item.hash === hash);
        if (i === -1) return;
        this.goToHistoryFwdByIndex(i);
      }
    };

    this.directory = new OneJournalDirectory(this);
  }

  open(attachApp: JournalSheet) {
    if (attachApp && this.detachedJournals.has(attachApp.entity.uuid)) {
      return;
    }
    this.render(true, { attachApp });
  }

  render(force?: boolean, options: any = {}): Application {
    const { attachApp, ...rest } = options;

    if (this._state <= 0) {
      Hooks.once("render" + this.constructor.name, () => {
        Hooks.once("render" + this.directory.constructor.name, () => {
          return this.onRenderComplete(attachApp);
        });
      });
      return super.render(force, rest) as JournalShell;
    } else {
      if (attachApp) {
        this.attach(attachApp);
      }
    }
    return this;
  }

  toggleSidebar(): void {
    this.element.toggleClass("sidebar-mode-none");
    setSetting(
      settings.SIDEBAR_COLLAPSED,
      !getSetting(settings.SIDEBAR_COLLAPSED)
    );
  }

  async onRenderComplete(attachApp?: any) {
    if (attachApp) {
      this.attach(attachApp);
    }
    const sidebarDisabled =
      !game.user.isGM && getSetting(settings.SIDEBAR_DISABLE_PLAYER);

    if (sidebarDisabled) {
      this.element.find(".one-journal-shell > .sidebar-toggle").remove();
      this.element.addClass("sidebar-disabled");
    } else {
      this.element
        .find(".one-journal-shell > .sidebar-toggle")
        .click(() => this.toggleSidebar());
    }

    if (getSetting(settings.SIDEBAR_COLLAPSED) === true || sidebarDisabled) {
      this.element.addClass("sidebar-mode-none");
    }
  }

  async close(): Promise<any> {
    // @ts-ignore
    const attachedClose = ui.windows[this.attachedId]?.close();

    this.restoreMaximized();
    await Promise.all([
      attachedClose,
      this.directory.close(true),
      super.close(),
    ]);
  }

  async minimize(): Promise<boolean> {
    this.restoreMaximized();
    return super.minimize();
  }

  activateListeners(html: JQuery) {
    const header = this.element.children(".window-header");
    const close = header.find(".close");
    close.attr("title", i18n("ApplicationExitTitle"));
    close.contents().last().replaceWith(i18n("ApplicationExit"));
    header.children().wrapAll(`<div class="one-journal-header" />`);
    this.directory.render(true);
    this.element.find(".sub-mode-toggle").click(() => {
      this.toggleSubMode();
    });
    this.element
      .find(".history-navigation .forward")
      .click(() => this.forward());
    this.element
      .find(".history-navigation .backward")
      .click(() => this.backward());

    this.changeSidebarMode(getSetting(settings.SIDEBAR_MODE));
    this.setSidebarWidth(getSetting(settings.SIDEBAR_WIDTH));

    this._historyContextMenu(html);
  }

  attach(app: BaseEntitySheet) {
    if (
      app.appId == this.attachedId ||
      this.detachedJournals.has(app.entity.uuid)
    ) {
      return;
    }
    if (this.attachedId != -1) {
      // @ts-ignore
      ui.windows[this.attachedId]?.close();
    }

    this.attachedId = app.appId;
    this.attachedUid = app.entity.uuid;
    this.element.addClass("journal-attached");
    if (getSetting(settings.FOLDER_SELECTOR) === true) {
      this.element.addClass("show-folder-select");
    }
    (app.element as JQuery).addClass("one-journal-attached");

    // Check if element is open in another window (PopOut!)
    if (this.document != document) {
      this.document.adoptNode((app.element as JQuery).get(0));
    }
    this.element.find(".shell-content").append(app.element);
    const headerContents = (app.element as JQuery)
      .find(".window-header > *")
      .detach();
    if (headerContents.length > 0) {
      const header = this.element.children(".window-header");
      header.children(":not(.one-journal-header)").remove();
      header.prepend(headerContents);
      header
        .children(".header-button")
        .removeClass("header-button")
        .addClass("journal-header-button")
        .on("click", e => {
          //@ts-ignore
          const buttons = app._getHeaderButtons();
          e.preventDefault();
          const button = buttons.find(b =>
            (e.currentTarget as HTMLElement).classList.contains(b.class)
          );
          button.onclick(e);
        });
    }

    this.directory.selected(this.attachedUid);
    this.navigated(app);

    // @ts-ignore
    let minimized = app._minimized;
    if (minimized) {
      // @ts-ignore
      app.maximize();
      this.minimize();
    }
  }

  detach(app: JournalSheet) {
    if (!this.swappingJournals.delete(app.entity.uuid)) {
      if (this.detachedJournals.delete(app.entity.uuid)) {
        this.directory.render(true);
      }
    }
    if (this.attachedId === app.appId) {
      this.element.removeClass("journal-attached");
      this.element
        .children(".window-header")
        .children(":not(.one-journal-header)")
        .remove();
      app.close();
      this.attachedId = -1;
      this.directory.deselected();
    }
  }

  async openDetached(uuid: string): Promise<void> {
    if (this.attachedUid === uuid) {
      //@ts-ignore
      ui.windows[this.attachedId]?.close();
      await new Promise(r => setTimeout(r, 300));
      this.detachedJournals.add(uuid);
    } else {
      this.detachedJournals.add(uuid);
    }
    this.directory.render(true);
    const e = await fromUuid(uuid);
    e.sheet.render(true);
  }

  fullScreen() {
    if (this.element.hasClass("maximized")) {
      this.element.removeClass("maximized");
      this.element
        .find(".maximize-toggle")
        .contents()
        .last()[0].textContent = i18n("ApplicationMaximize");
      this.element
        .find(".maximize-toggle i")
        .removeClass("fa-compress-arrows-alt")
        .addClass("fa-expand-arrows-alt");
      $(document.body).removeClass("one-journal-sub-mode");
    } else {
      this.element.addClass("maximized");
      this.element
        .find(".maximize-toggle")
        .contents()
        .last()[0].textContent = i18n("ApplicationRestore");
      this.element
        .find(".maximize-toggle i")
        .removeClass("fa-expand-arrows-alt")
        .addClass("fa-compress-arrows-alt");
    }
  }

  toggleSubMode() {
    $(document.body).toggleClass("one-journal-sub-mode");
  }

  restoreMaximized() {
    if (this.element.hasClass("maximized")) {
      this.fullScreen();
    }
  }

  _getHeaderButtons() {
    let buttons = super._getHeaderButtons();
    buttons.unshift({
      label: i18n("ApplicationMaximize"),
      class: "maximize-toggle",
      icon: "fas fa-expand-arrows-alt",
      onclick: async ev => {
        this.fullScreen();
      },
    });
    return buttons;
  }

  pushWindowHistory(item: IHistoryItem) {
    if (getSetting(settings.USE_BROWSER_HISTORY) !== true) return;
    window.location.hash = item.hash;
  }

  // History
  navigated(app: BaseEntitySheet) {
    const navigatedTo = app.entity.uuid;
    if (this.history.length !== 0) {
      if (this.history[this.history.length - 1].id === navigatedTo) {
        // Already last in history
        return;
      } else {
        // Reset forward stack
        this.historyFwd.length = 0;
      }
    }
    if (
      getSetting(settings.NO_DUPLICATE_HISTORY) !== true &&
      getSetting(settings.USE_BROWSER_HISTORY) !== true
    ) {
      this.history = this.history.filter(h => h.id !== navigatedTo);
    }

    this.history.push({
      id: navigatedTo,
      title: app.title,
      hash: `#OJ${this.historySeq++}`,
    });
    this.pushWindowHistory(this.history[this.history.length - 1]);
    this.updatedHistory();
  }

  async backward() {
    if (getSetting(settings.USE_BROWSER_HISTORY) === true) {
      if (this.history.length > 1) window.history.back();
      return;
    }

    if (this.history.length <= 1) {
      this.updatedHistory();
      return;
    }
    if (this.attachedId !== -1) {
      this.historyFwd.push(this.history.pop());
    }
    const entity = await fromUuid(this.history[this.history.length - 1].id);
    if (entity === null) {
      // Remove and retry
      this.history.pop();
      await this.backward();
    } else {
      entity.sheet.render(true);
      this.updatedHistory();
    }
  }

  async forward() {
    if (getSetting(settings.USE_BROWSER_HISTORY) === true) {
      if (this.historyFwd.length != 0) window.history.forward();
      return;
    }

    if (this.historyFwd.length == 0) {
      this.updatedHistory();
      return;
    }
    this.history.push(this.historyFwd.pop());
    const entity = await fromUuid(this.history[this.history.length - 1].id);
    if (entity === null) {
      // Remove and retry
      this.history.pop();
      this.forward();
    } else {
      entity.sheet.render(true);
      this.updatedHistory();
    }
  }

  async goToHistoryByIndex(index: number) {
    const entity = await fromUuid(this.history[index].id);

    while (this.history.length > index + 1) {
      this.historyFwd.push(this.history.pop());
    }
    entity.sheet.render(true);
    this.updatedHistory();
  }

  async goToHistoryFwdByIndex(index: number) {
    const entity = await fromUuid(this.historyFwd[index].id);

    while (this.historyFwd.length > index) {
      this.history.push(this.historyFwd.pop());
    }
    entity.sheet.render(true);
    this.updatedHistory();
  }

  clearHistory() {
    this.history.length = 0;
    this.historyFwd.length = 0;
    this.updatedHistory();
  }

  updatedHistory() {
    const historyItems = this.history.map((item, idx) => {
      return {
        name: item.title,
        icon: `<i class="fas fa-arrow-left"></i>`,
        callback: () => {
          if (getSetting(settings.USE_BROWSER_HISTORY) === true) {
            for (let i = 0; i < this.history.length - 1 - idx; i++) {
              window.history.back();
            }
            return;
          }
          this.goToHistoryByIndex(idx);
        },
      };
    });

    const historyFwdItems = this.historyFwd.map((item, idx) => {
      return {
        name: item.title,
        icon: `<i class="fas fa-arrow-right"></i>`,
        callback: () => {
          if (getSetting(settings.USE_BROWSER_HISTORY) === true) {
            for (let i = 0; i < this.historyFwd.length - idx; i++) {
              window.history.forward();
            }
            return;
          }
          this.goToHistoryFwdByIndex(idx);
        },
      };
    });

    if (historyItems.length > 0) {
      historyItems[
        historyItems.length - 1
      ].icon = `<i class="fas fa-circle"></i>`;
    }

    this.contextMenu.menuItems = [
      ...historyItems,
      ...historyFwdItems.reverse(),
    ].reverse();

    if (getSetting(settings.USE_BROWSER_HISTORY) !== true) {
      this.contextMenu.menuItems.unshift({
        name: i18n("HistoryClearHistory"),
        icon: '<i class="fas fa-trash"></i>',
        callback: li => {
          this.clearHistory();
        },
      });
    } else {
      this.contextMenu.menuItems.unshift({
        name: i18n("HistoryUsingBrowserHistory"),
        icon: "",
        callback: li => {},
      });
    }

    if (this.history.length > 1) {
      this.element.find(".history-navigation .backward").addClass("active");
    } else {
      this.element.find(".history-navigation .backward").removeClass("active");
    }
    if (this.historyFwd.length !== 0) {
      this.element.find(".history-navigation .forward").addClass("active");
    } else {
      this.element.find(".history-navigation .forward").removeClass("active");
    }
  }

  changeSidebarMode(mode: string) {
    if (mode === "left") {
      this.element.addClass("sidebar-mode-left");
    } else {
      this.element.removeClass("sidebar-mode-left");
    }
  }

  setSidebarWidth(width: number) {
    document.documentElement.style.setProperty(
      "--ojSidebarWidth",
      `${width}px`
    );
  }

  _historyContextMenu(html) {
    this.contextMenu = new ContextMenu(html, ".history-navigation", [
      {
        name: i18n("HistoryClearHistory"),
        icon: '<i class="fas fa-trash"></i>',
        callback: li => {
          this.clearHistory();
        },
      },
    ]);
  }
}

// @ts-ignore
class OneJournalDirectory extends JournalDirectory {
  get element(): JQuery<HTMLElement> {
    return super.element as JQuery<HTMLElement>;
  }
  shell: JournalShell;
  constructor(shell, options?) {
    super(options);
    this.shell = shell;
    // Record the directory as an application of the collection
    OneJournalDirectory.collection.apps.push(this);
  }
  static get defaultOptions() {
    const options = super.defaultOptions;
    options.id = "OneJournalDirectory";
    options.template = "templates/sidebar/journal-directory.html";
    options.popOut = true;
    return options;
  }

  static get entity() {
    return "JournalEntry";
  }

  static get collection() {
    return game.journal;
  }

  render(force?: boolean, options?: any): SidebarDirectory {
    if (this.shell._state <= 0) {
      return;
    }
    return super.render(force, options);
  }

  close(force?: boolean): Promise<any> {
    if (force) {
      return Application.prototype.close.call(this);
    }
    // Close the entire shell if someone tries to close directory
    return this.shell.close();
  }

  selected(uuid: string) {
    const [_, id] = uuid.split(".");
    this.element.find("li.selected").removeClass("selected");
    const selected = this.element.find(`li[data-entity-id="${id}"]`);
    selected.addClass("selected");
    if (getSetting(settings.SYNC_SIDEBAR) === false) {
      return;
    }
    this.expandFolderTree(selected);
    scrollElementIntoView(selected, this.element.find(".directory-list"));
  }

  deselected() {
    this.element.find("li.selected").removeClass("selected");
  }

  expandFolderTree(target: JQuery<HTMLElement>) {
    target.parents(".folder").removeClass("collapsed");
  }

  expand(id: string, expanded: boolean) {
    const li = this.element.find(`li[data-folder-id="${id}"]`);
    if (expanded) {
      li.removeClass("collapsed");
    } else {
      li.addClass("collapsed");
      li.find(".folder").addClass("collapsed");
    }
    const expandedFolders = this.element.find(
      ".directory-list > .folder:not(.collapsed)"
    );
    if (expandedFolders.length === 0) {
      this.element.removeClass("has-expanded-journals");
    } else {
      this.element.addClass("has-expanded-journals");
    }
  }

  activateListeners(html: JQuery): void {
    super.activateListeners(html);
    this.shell.element.find(".shell-sidebar").append(this.element);
    if (this.shell.attachedId !== -1 && this.shell.attachedUid) {
      this.selected(this.shell.attachedUid);
    }
    let toggleSibling = this.element.find(".header-actions .create-folder");
    if (toggleSibling.length === 0) {
      toggleSibling = this.element.find(".header-search .collapse-all");
    }
    toggleSibling.after(
      `<div class="sidebar-toggle" title="${i18n(
        "SidebarCollapse"
      )}"><i class="far fa-window-maximize"></i></div>`
    );

    this.element.find(".sidebar-toggle").click(() => {
      this.shell.toggleSidebar();
    });
    if (getSetting(settings.SIDEBAR_FOLDER_COLOR) === true) {
      this.element.find("header.folder-header").each((i, el) => {
        if (el.style.backgroundColor) {
          (el.nextElementSibling as HTMLElement).style.borderColor =
            el.style.backgroundColor;
        }
      });
    }

    this.setSidebarCompact(getSetting(settings.SIDEBAR_COMPACT));

    this.element.find(".entity-name .fa-external-link-alt").remove();
    this.shell.detachedJournals.forEach(uuid => {
      const [_, id] = uuid.split(".");
      this.element
        .find(`[data-entity-id="${id}"]`)
        .addClass("journal-detached")
        .find(`h4`)
        .attr("title", i18n("JournalEntryDetached"))
        .append(`<i class="fas fa-external-link-alt"></i>`);
    });
  }

  setSidebarCompact(on: boolean) {
    if (on) {
      this.element.addClass("compact");
    } else {
      this.element.removeClass("compact");
    }
  }

  _getEntryContextOptions() {
    // @ts-ignore
    const options = super._getEntryContextOptions();
    return options.concat([
      {
        name: "SIDEBAR.JumpPin",
        icon: '<i class="fas fa-crosshairs"></i>',
        condition: li => {
          const entry = game.journal.get(li.data("entity-id"));
          return !!entry.sceneNote;
        },
        callback: li => {
          const entry = game.journal.get(li.data("entity-id"));
          entry.panToNote();
        },
      },
      {
        name: "ONEJOURNAL.OptionOpenDetached",
        icon: `<i class="fas fa-external-link-alt"></i>`,
        callback: li => {
          const entry = game.journal.get(li.data("entity-id"));
          this.shell.openDetached(entry.uuid);
        },
      },
    ]);
  }
}
